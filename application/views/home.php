<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$user_login	= $this->session->userdata();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aplikasi Sederhana</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
		$( function() 
		{
			$( "#tgl_awal" ).datepicker({dateFormat: "yy-mm-dd"});
			$( "#tgl_akhir" ).datepicker({dateFormat: "yy-mm-dd"});
			$( "#tgl_lahir" ).datepicker({dateFormat: "dd-MM-yy"});
		} 
		);
	</script>
</head>
<body>
	<header class="header">
        	<?php
            	if($user_login['tipe'] == "1")
				{
					$this->load->view('tamplate/view_menu');
				}
				else
				{
					$this->load->view('tamplate/view_menu_user');	
				}
			?>
            </br>
            <!--ini isi form-->
            <?php
            	$this->load->view($content);
			?>
    </header>
</body>
</html>