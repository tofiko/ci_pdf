<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "penjualan_header";
	private $_table_detail = "penjualan_detail";
	
	public function tampilDataPenjualan()
	{
		$query = $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1" );
		return $query->result();
	}
	
	public function notransaksi()
	{
		$query = $this->db->query("SELECT * FROM penjualan_header ORDER by no_transaksi DESC LIMIT 1");
		return $query->result();
	}
	
	public function rulesH()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'pembeli',
			'label'		=> 'Nama Pembeli',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Pembeli Tidak Boleh Kosong.'
							]
			]
		];	
	}
	
	public function savePenjualanHeader()
	{
		$data['no_transaksi']	= $this->input->post('no_transaksi');
		$data['pembeli']	= $this->input->post('pembeli');
		$data['tanggal']		= date('Y-m-d');
		$data['flag']			= 1;
		$this->db->insert($this->_table_header, $data);
	}
	
	public function idTransaksiTerakhir()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY
			id_jual_h DESC LIMIT 0,1" );
			$data_id = $query->result();
			
			foreach ($data_id as $data){
				$last_id = $data->id_jual_h;
			}
		return $last_id;
	}
	
	Public function tampilDataPenjualanDetail($id_penjualan_header)
	{			
		$query = $this->db->query(
			"SELECT A. *,  B.nama_barang FROM " . $this->_table_detail . " AS A
			INNER JOIN `barang` AS B ON A.kode_barang = B.kode_barang
			WHERE A.`flag` = '1' AND A.`id_jual_h`  = ". $id_penjualan_header 
			);
			$data = $query->result();
			
			
		return $query->result();
	}
	
	public function rulesD()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'qty',
			'label'		=> 'QTY',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'QTY Tidak Boleh Kosong.',
								'numeric'		=>	'QTY harus berisi angka.'
							]
			]
			/*[
			'field' 	=> 'harga',
			'label'		=> 'Harga Barang',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'Harga Barang Tidak Boleh Kosong.',
								'numeric'		=>	'Harga Barang harus berisi angka.'
							]
			]*/
		];	
	}
	
	public function savePenjualanDetail($id_penjualan_header)
	{
		$harga = $this->input->post('harga');
		$qty = $this->input->post('qty');
		
		$data['id_jual_h']		= $id_penjualan_header;
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['qty']			= $qty;
		$data['harga']			= $harga;
		$data['jumlah']			= $harga * $qty;
		$data['flag']			= 1;
		$this->db->insert($this->_table_detail, $data);
	}
	
	/*public function tampilreportpembelian($tgl_awal, $tgl_akhir)
	{
		
		$this->db->select("A.id_pembelian_h, A.no_transaksi, A.tanggal,
		COUNT(B.kode_barang) AS total_barang, SUM(B.qty) AS total_qty, SUM(B.jumlah) AS total_pembelian");
		$this->db->from("pembelian_header as A");
		$this->db->join("pembelian_detail AS B", "A.id_pembelian_h = B.id_pembelian_h");
		$this->db->where("A.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");	
		$this->db->group_by("A.id_pembelian_h");
		
		$query = $this->db->get();
		return $query->result();
	}*/
}
